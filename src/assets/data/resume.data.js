const RESUME_DATA = {
    born:{
        id:1,
        name:'Born',
        data:[
            {
                id:2,
                year:2000,
                info:'เกิดวันที่ 11 กุมภาพันธ์ '
            }
        ]
    },
    education:{
        id:3,
        name:'Education',
        data:[
            {
                id:4,
                year:2007,
                info:'เข้าเรียนที่ .... ในระดับชั้น ...'
            },
            {
                id:5,
                year:2012,
                info:'เข้าเรียนที่ .... ในระดับชั้น ...'
            },
            {
                id:6,
                year:2018,
                info:'เข้าเรียนที่ .... ในระดับชั้น ...'
            },
        ]
    },
    experience:{
        id:7,
        name:"Experiences",
        data:[
            {
                id:8,
                year:2013,
                info:'ทำงานที่ ....'
            },
            {
                id:9,
                year:2015,
                info:'ทำงานที่ ....'
            },
            {
                id:10,
                year:2019,
                info:'ทำงานที่ ....'
            }
        ]
    },
    awards:{
        id:11,
        name:'Awards',
        data:[
            {
                id:12,
                year:2019,
                info:'ได้รับรางวัล....'
            },
            {
                id:13,
                year:2020,
                info:'ได้รับรางวัล....'
            },
           
        ]
    },
    skills:{
        id:14,
        name:'Skills',
        data:[
            {
                id:15,
                skill_name:'Javascript',
                skill_icon:'fab fa-js',
                color:'icon-yellow'
            },
            {
                id:16,
                skill_name:'HTML',
                skill_icon:'fab fa-html5',
                color:'icon-orange'
            },
            {
                id:17,
                skill_name:'CSS',
                skill_icon:'fab fa-css3-alt',
                color:'icon-blue'
            },
            {
                id:18,
                skill_name:'scss',
                skill_icon:'fab fa-sass',
                color:'icon-pink'
            },
            {
                id:19,
                skill_name:'PHP',
                skill_icon:'fab fa-php',
                color:'icon-purple'
            },
            {
                id:20,
                skill_name:'Python',
                skill_icon:'fab fa-python',
                color:'icon-lightblue'
            },
            {
                id:21,
                skill_name:'react',
                skill_icon:'fab fa-react',
                color:'icon-skyblue'
            },
        ]
    }
}


export default RESUME_DATA;