import React from 'react';

const Footer = () => {
    return(
        <footer className='py-1 bg-dark'>
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <div className="py-4 text-center">
                            <p className='text-white mb-0'>© 2022 Portfolio. Design by nammon </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer;